extern crate httpserve;
use httpserve::ThreadPool;

use std::io::prelude::*;
use std::fs;
use std::net::TcpStream;
use std::net::TcpListener;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn main() {
    let addr = "127.0.0.1:7878";
    let listener = TcpListener::bind(&addr).unwrap();
    println!("Listening on http://{}", &addr);

    let counter = Arc::new(Mutex::new(0usize));
    let tasks = ThreadPool::new(100);

    for stream in listener.incoming()/*.take(2)*/ {
        println!("accepted new connection");

        if let Ok(stream) = stream {
            let counter = Arc::clone(&counter);

            tasks.run(move || {
                handle_connection(counter, stream);
            });
        }
    }

    println!("Shutting down...");
}

fn handle_connection(counter: Arc<Mutex<usize>>, mut stream: TcpStream) {
    let mut buffer = [0; 512];

    stream.read(&mut buffer).unwrap();

    {
        // in a subscope to unlock automatically
        let mut count = counter.lock().unwrap();
        *count += 1;
        println!("Request: {}\n{}", count, String::from_utf8_lossy(&buffer[..]));
    }

    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";

    let (headers, response) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK\r\n\r\n", "index.html")
    } else if buffer.starts_with(sleep) {
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK\r\n\r\n", "index.html")
    } else {
        ("HTTP/1.1 404 Not Found\r\n\r\n", "404.html")
    };

    let response = fs::read(format!("public/{}", response)).unwrap();
    let response = &response[..];
    stream.write(headers.as_bytes()).unwrap();
    stream.write(response).unwrap();
    stream.flush().unwrap();
}
